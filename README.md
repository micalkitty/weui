这是一个seajs+angular+weui的单页按需加载应用，使用coffee-script jade stylus开发，js遵循规范 cmd规范。话不多说，下面教你跑起来的方法和开发方法。

运行方法：

1. git clone https://git.oschina.net/hlmcdh/weui.git

2. cd weui

3. npm install

4. npm start

5. 浏览器打开 http://localhost:3000

开发方法：

1、cd weui/src/public 新建ng控制器coffee文件 + 视图jade文件, 可复制已有的控制器代码。

2、cd weui 执行gulp 可实时编译到 dest目录下

3、刷新页面可看到效果