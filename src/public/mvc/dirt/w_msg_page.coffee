define (require, exports, module) ->
  app = require 'app'
  msgPageFunc = ($timeout) ->
    return {
      restrict: "E"
      transclude: true
      scope:
        msgtitle: '='
        msgtype: '='
        msgdesc: '='
        okclk: '&'
        cancelclk: '&'
        detailurl: '='
        cbf: "&"
      templateUrl: 'tpl/msg_page_tpl.html'
      link: (scope, element, attrs) ->
    }
  msgPageFunc.$inject = ['$timeout']
  app.register.directive 'wmsgpage', msgPageFunc