define (require, exports, module) ->
  app = require 'app'
  dialogFunc = ($timeout) ->
    return {
      restrict: "E"
      transclude: true
      scope: 
        ctitle: '='
        cdlgtype: '='
        okclk: '&'
        cancelclk: '&'
        cstatus: '='
        cbf: "&"
      templateUrl: 'tpl/dialog_tpl.html'
      link: (scope, element, attrs) ->

    }
  dialogFunc.$inject = ['$timeout']
  app.register.directive 'wdialog', dialogFunc