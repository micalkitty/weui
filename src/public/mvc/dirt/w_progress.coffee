define (require, exports, module) ->
  app = require 'app'
  progressFunc = ($timeout) ->
    return {
      restrict: "E"
      transclude: true
      scope:
        cpercent: '='
        cbf: "&"
      templateUrl: 'tpl/progress_tpl.html'
      link: (scope, element, attrs) ->
        scope.pgCancel = () ->
          if scope.cbf
            return scope.cbf()
          else
            return false
    }
  progressFunc.$inject = ['$timeout']
  app.register.directive 'wprogress', progressFunc

