define (require, exports, module) ->
  app = require 'app'
  actionSheetFunc = ($timeout) ->
    return {
      restrict: "E"
      transclude: true
      scope:
        actionitems: '='
        astatus: '='
        cbf: "&"
      templateUrl: 'tpl/action_sheet_tpl.html'
      link: (scope, element, attrs) ->
        scope.item_clk = (idx) ->
          if scope.cbf
            return scope.cbf 'action_index' : idx
    }
  actionSheetFunc.$inject = ['$timeout']
  app.register.directive 'wactionsheet', actionSheetFunc