define (require, exports, module) ->
  app = require 'app'
  gridFunc = ($timeout) ->
    return {
      restrict: "E"
      transclude: true
      scope: 
        griditems: '='
        cbf: "&"
      templateUrl: 'tpl/grid_tpl.html'
      link: (scope, element, attrs) ->

    }
  gridFunc.$inject = ['$timeout']
  app.register.directive 'wgrid', gridFunc