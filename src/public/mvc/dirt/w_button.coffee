define (require, exports, module) ->
  app = require 'app'
  buttonFunc = ($timeout) ->
    return {
      restrict: "E"
      transclude: true
      scope: 
        ctitle: '='
        cstyle: '='
        cinner: '='
        cbf: "&"
      templateUrl: 'tpl/button_tpl.html'
      link: (scope, element, attrs) ->
        # 按钮点击触发
        scope.btnClk = () ->
          if 'b' is '' + scope.cstyle or 'd' is '' + scope.cstyle or 'f' is '' + scope.cstyle
            return false
          else
            if scope.cbf
              return scope.cbf()
            else
              return false
    }
  buttonFunc.$inject = ['$timeout']
  app.register.directive 'wbutton', buttonFunc