define (require, exports, module) ->
  app = require 'app'
  viewFunc = ($timeout, $window) ->
    return {
      restrict: "E"
      transclude: true
      scope: 
        ctitle:'='
        cbuttons:'='
        cbf: "&"
      templateUrl: 'tpl/header_tpl.html'
      link: (scope, element, attrs) ->
        scope.headerBtnClk = (url) ->
          if url and "" isnt url
            $window.location.href = url

    }
  viewFunc.$inject = ['$timeout', '$window']
  app.register.directive 'wheader', viewFunc