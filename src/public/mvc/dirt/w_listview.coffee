define (require, exports, module) ->
  app = require 'app'
  viewFunc = ($timeout) ->
    return {
      restrict: "E"
      transclude: true
      scope: 
        clisttype:'='
        clistitems:'='
        cbf: "&"
      templateUrl: 'tpl/listview_tpl.html'
      link: (scope, element, attrs) ->

    }
  viewFunc.$inject = ['$timeout']
  app.register.directive 'wlistview', viewFunc