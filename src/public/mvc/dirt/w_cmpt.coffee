define (require, exports, module) ->
  app = require 'app'
  wboxFunc = ($timeout) ->
    return {
      restrict: 'E'
      transclude: true
      scope:
        ctype: '='
        citems: '='
        clabel: '='
        cfieldname: '='
        checkitems: '='
        radioitems: '='
        selectitems: '='
        switchitems: '='
        textitems: '='
        vcodeitems: '='
        vimgurl: '@'
        textareaitems: '='
        dateitems: '='
        cplaceholder: '='
        clisttype: '='
        clistitems: '='
        iscell:'='
        cbf: '&'
      templateUrl: 'tpl/cmpt_tpl.html'

      link: (scope, element, attrs) ->
        # 如果是复选框，则当前数组需初始化
        if 'checkbox' is '' + scope.ctype
          checkLen = scope?.checkitems?.length or 0
          if 0 is checkLen
            scope.checkitems = scope.citems

        # 查找对象数组中键为key的对象
        scope.findByKey = (arr, key) ->
          len = arr?.length or 0
          if 0 is len or not key
            return null

          for a in arr
            if '' + key is '' + a['key']
              return a
          return

        # 复选框点击
        scope.checkClick = ->
          return scope.cbf 'check_data': scope.checkitems

        # 单选框点击
        scope.radioClick = ->
          radioObj = scope.findByKey scope.citems, scope.radioitems
          return scope.cbf 'radio_data': radioObj

        # 下拉框选择
        scope.selectChange = ->
          selectObj = scope.findByKey scope.citems, scope.selectitems
          return scope.cbf 'select_data': selectObj

        # 开关点击
        scope.switchClick = ->
          return scope.cbf 'switch_data': scope.switchitems

        # 文本框输入
        scope.textChange = ->
          return scope.cbf 'text_data': scope.textitems

        # 验证码输入
        scope.vcodeChange = ->
          return scope.cbf 'vcode_data': scope.vcodeitems

        # 多行文本框输入
        scope.textareaChange = ->
          return scope.cbf 'textarea_data': scope.textareaitems

        # 多行文本框输入
        scope.dateChange = ->
          return scope.cbf 'date_data': scope.dateitems
    }

  wboxFunc.$inject = ['$timeout']
  app.register.directive 'wbox', wboxFunc
