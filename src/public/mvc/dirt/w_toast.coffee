define (require, exports, module) ->
  app = require 'app'
  toastFunc = ($timeout) ->
    return {
      restrict: "E"
      transclude: true
      scope: 
        ctoasttype: '='
        cstatus: '='
        cbf: "&"
      templateUrl: 'tpl/toast_tpl.html'
      link: (scope, element, attrs) ->

    }
  toastFunc.$inject = ['$timeout']
  app.register.directive 'wtoast', toastFunc