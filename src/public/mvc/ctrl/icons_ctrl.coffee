define (require, exports, module) ->
  # 加载ng模块实例
  app = require 'app'

  # 控制器函数
  iconsFunc = ($scope, $http, $location, $timeout, appServ) ->

    # 定义变量
    $scope.vars =
      # 顶部导航条
      header_btns: []

    # 定义方法
    $scope.act =

      # 初始化
      init: () ->
        # 头部按钮
        $scope.vars.header_btns = [
          {'title': '返回', 'goto_url' : '#/'}
        ]

        # 转圈loadding关闭
        appServ.loadOk()

    # 控制器入口
    return $scope.act.init()

  # 注入模块
  iconsFunc.$inject = ['$scope', '$http', '$location', '$timeout', 'appServ']
  # 注册控制器
  app.register.controller 'iconsCtrl', iconsFunc