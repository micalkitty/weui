define (require, exports, module) ->
  # 加载ng模块实例
  app = require 'app'

  # 控制器函数
  progressFunc = ($scope, $http, $location, $timeout, $interval,appServ) ->

    # 定义变量
    $scope.vars =
      # 顶部导航条
      header_btns: []
      # 第一组百分比
      percent1: 0
      # 第二组百分比
      percent2: 50
      # 第三组百分比
      percent3: 80

    # 定义方法
    $scope.act =

      upload: () ->
        $scope.vars.percent1 = 0
        $scope.vars.percent2 = 0
        $scope.vars.percent3 = 0
        $interval () ->
          if $scope.vars.percent1 < 100
            $scope.vars.percent1 += 1
          else
            $scope.vars.percent1 = 0
        , 30

        $interval () ->
          if $scope.vars.percent2 < 100
            $scope.vars.percent2 += 1
          else
            $scope.vars.percent2 = 0
        , 30

        $interval () ->
          if $scope.vars.percent3 < 100
            $scope.vars.percent3 += 1
          else
            $scope.vars.percent3 = 0
        , 30

      # 初始化
      init: () ->
        # 头部按钮
        $scope.vars.header_btns = [
          {'title': '返回', 'goto_url' : '#/'}
        ]

        # 转圈loadding关闭
        appServ.loadOk()

    # 控制器入口
    return $scope.act.init()

  # 注入模块
  progressFunc.$inject = ['$scope', '$http', '$location', '$timeout', '$interval', 'appServ']
  # 注册控制器
  app.register.controller 'progressCtrl', progressFunc