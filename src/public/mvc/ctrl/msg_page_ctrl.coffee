define (require, exports, module) ->
  # 加载ng模块实例
  app = require 'app'

  # 控制器函数
  msgPageFunc = ($scope, $http, $location, $timeout, appServ) ->

    # 定义变量
    $scope.vars =
      # 顶部导航条
      header_btns: []

    # 定义方法
    $scope.act =

      # 消息页确认
      msgOkClk : () ->
        console.log 'click msg ok'
        $scope.vars.msgStatus = false

      # 消息页取消
      msgCancelClk : () ->
        console.log 'click msg cancel'
        $scope.vars.msgStatus = false

      # 初始化
      init: () ->
        # 头部按钮
        $scope.vars.header_btns = [
          {'title': '返回', 'goto_url' : '#/'}
        ]

        # 转圈loadding关闭
        appServ.loadOk()

    # 控制器入口
    return $scope.act.init()

  # 注入模块
  msgPageFunc.$inject = ['$scope', '$http', '$location', '$timeout', 'appServ']
  # 注册控制器
  app.register.controller 'msgPageCtrl', msgPageFunc