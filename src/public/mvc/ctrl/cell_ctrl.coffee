define (require, exports, module) ->
  # 加载ng模块实例
  app = require 'app'
  # 控制器函数
  cellFunc = ($scope, $http, $location, $timeout, cellServ, appServ) ->
    # 定义变量
    $scope.vars =
      # 联系方式的值
      lxfs_val: {}
      # 联系方式列表
      lxfs_items: {}

      # 性别的值
      sex_val: {}
      # 性别列表
      sex_items: {}

      # 爱好的值
      love_val: {}
      # 爱好列表
      love_items: {}

      # 开关的值
      switch_val: false
      # qq号码的值
      qq_val: ''
      # 验证码的值
      vcode_val: ''
      # 备注的值
      remark_val: ''
      # 日期的值
      rq_val: ''
      # 产品列表
      prod_items: {}
      # 头部按钮
      header_btns : []

    # 定义方法
    $scope.act =

      # 联系方式 - 下拉回调
      lxfsChange: (val) ->
        console.log 'lxfs:', val

      # 性别 - 选中回调
      sexChange: (val) ->
        console.log 'sex:', val

      # 性别 - 选中回调
      loveChange: (val) ->
        console.log 'love:', JSON.stringify val

      # 开关 - 选中回调
      switchChange: (val) ->
        console.log 'switch:', JSON.stringify val

      # 文本 - 输入回调
      qqChange: (val) ->
        console.log 'qq:', JSON.stringify val

      # 验证码 - 选中回调
      vcodeChange: (val) ->
        console.log 'vcode:', JSON.stringify val

      # 备注 - 输入回调
      remarkChange: (val) ->
        console.log 'remark:', JSON.stringify val

      # 备注 - 输入回调
      rqChange: (val) ->
        console.log 'rq:', JSON.stringify val

      # 初始化
      init: () ->
        # 联系方式 - 选中的值
        $scope.vars.lxfs_val = {}
        # 下拉框 - 组件数据
        $scope.vars.lxfs_items = [
          {'key': '1', 'value': '微信号', 'group': '1'},
          {'key': '2', 'value': 'QQ号', 'group': '2'},
          {'key': '3', 'value': 'Email', 'group': '3'}
        ]

        # 性别 - 选中的值
        $scope.vars.sex_val = {'key': '1', 'value': '男', 'group': '1'}
        # 单选框 - 组件数据
        $scope.vars.sex_items = [
          {'key': '1', 'value': '男', 'group': '1'}
          {'key': '2', 'value': '女', 'group': '2'}
        ]

        # 爱好 - 选中的值
        $scope.vars.love_val = []
        # 复选框 - 组件数据
        $scope.vars.love_items = [
          {'key': '1', 'value': '旅游', 'group': '1', 'checked': false}
          {'key': '2', 'value': '篮球', 'group': '2', 'checked': false}
          {'key': '3', 'value': '足球', 'group': '3', 'checked': false}
        ]

        # 产品列表 - 列表视图数据
        $scope.vars.prod_items = [
          {'title': '产品1', 'remark': '介绍产品1', 'img_url': '', 'goto_url': ''}
          {'title': '产品2', 'remark': '介绍产品2', 'img_url': '', 'goto_url': ''}
          {'title': '产品3', 'remark': '介绍产品3', 'img_url': '', 'goto_url': ''}
        ]

        # 头部按钮数据
        $scope.vars.header_btns = [
          {'title': '返回', 'goto_url' : '#/'}
        ]
        # 通过服务获取QQ的值
        $scope.vars.qq_val = cellServ.getQqVal()
        # 转圈loadding关闭
        appServ.loadOk()

    return $scope.act.init()

  # 注入模块
  cellFunc.$inject = ['$scope', '$http', '$location', '$timeout', 'cellServ', 'appServ']
  # 注册控制器
  app.register.controller 'cellCtrl', cellFunc