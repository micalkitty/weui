define (require, exports, module) ->
  # 加载ng模块实例
  app = require 'app'

  # 控制器函数
  dialogFunc = ($scope, $http, $location, $timeout, appServ) ->

    # 定义变量
    $scope.vars =
      # 顶部导航条
      header_btns: []
      # 对话框状态
      dlgStatus:  false
      # 对话框状态2
      dlgStatus2: false

    # 定义方法
    $scope.act =

      # 显示对话框
      showDlg : () ->
        $scope.vars.dlgStatus = true

      # 点击确定处理
      ok : () ->
        console.log 'click ok'
        $scope.vars.dlgStatus = false

      # 点击取消处理
      cancel : () ->
        $scope.vars.dlgStatus = false
        console.log 'click cancel'

      # 显示对话框
      showDlg2 : () ->
        $scope.vars.dlgStatus2 = true

      # 点击确定处理
      ok2 : () ->
        console.log 'click ok'
        $scope.vars.dlgStatus2 = false

      # 点击取消处理
      cancel2 : () ->
        $scope.vars.dlgStatus2 = false
        console.log 'click cancel'

      # 初始化
      init: () ->
        # 头部按钮
        $scope.vars.header_btns = [
          {'title': '返回', 'goto_url' : '#/'}
        ]
        # 转圈loadding关闭
        appServ.loadOk()

    # 控制器入口
    return $scope.act.init()

  # 注入模块
  dialogFunc.$inject = ['$scope', '$http', '$location', '$timeout', 'appServ']
  # 注册控制器
  app.register.controller 'dialogCtrl', dialogFunc