define (require, exports, module) ->
  # 加载ng模块实例
  app = require 'app'

  # 控制器函数
  actionSheetFunc = ($scope, $http, $location, $timeout, appServ) ->

    # 定义变量
    $scope.vars =
      # 顶部导航条
      header_btns: []

      # 底部菜单状态
      asStatus : false
      # 底部弹出菜单
      asMenus : []

    # 定义方法
    $scope.act =

      # 显示底部弹出菜单
      showActionSheet : () ->
        $scope.vars.asStatus = true

      # 菜单点击
      actionSheetClk : (idx) ->
        console.log 'idx:', idx

      # 初始化
      init: () ->
        # 头部按钮
        $scope.vars.header_btns = [
          {'title': '返回', 'goto_url' : '#/'}
        ]
        $scope.vars.asMenus = [
          {'title': '示例菜单'}
          {'title': '示例菜单'}
          {'title': '示例菜单'}
          {'title': '示例菜单'}
        ]
        # 转圈loadding关闭
        appServ.loadOk()

    # 控制器入口
    return $scope.act.init()

  # 注入模块
  actionSheetFunc.$inject = ['$scope', '$http', '$location', '$timeout', 'appServ']
  # 注册控制器
  app.register.controller 'actionSheetCtrl', actionSheetFunc