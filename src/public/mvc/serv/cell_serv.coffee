define (require, exports, module) ->
  # 加载ng模块实例
  app = require 'app'
  # 服务函数
  cellFunc = ($http, $location, $timeout) ->
    service =
      getQqVal : () ->
        return '254475842'
    return service

  cellFunc.$inject = ['$http', '$location', '$timeout']
  # 控制器函数
  app.register.service 'cellServ', cellFunc