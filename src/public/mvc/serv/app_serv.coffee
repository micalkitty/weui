define (require, exports, module) ->
  # 加载ng模块实例
  app = require 'app'
  $ = require 'zepto'
  # 服务函数
  appFunc = ($http, $location, $timeout) ->
    service =
      # 加载成功，隐藏loadding
      loadOk: () ->
        $('#app_load').hide()
        return
    return service

  appFunc.$inject = ['$http', '$location', '$timeout']
  # 控制器函数
  app.register.service 'appServ', appFunc