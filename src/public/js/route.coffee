define (require, exports, module) ->
  # 引入ng实例
  app = require 'app'
  # 配置路由
  app.config ($routeProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $httpProvider) ->
    # 注册组件
    app.register = 
      controller: $controllerProvider.register
      directive: $compileProvider.directive
      filter: $filterProvider.register
      factory: $provide.factory
      service: $provide.service

    # 依赖的资源数组
    getDepsArray = (deps) ->
      depsArray = []
      if not deps
        return []

      # 加载指令
      dirtArr = deps?.direct or null
      dirtLen = dirtArr?.length or 0
      if 0 isnt dirtLen
        for currDirt in dirtArr
          if currDirt?.direct_path
            depsArray.push currDirt.direct_path

      # 加载服务
      servArr = deps?.service or null
      servLen = servArr?.length or 0
      if 0 isnt servLen
        for currServ in servArr
          if currServ?.service_path
            depsArray.push currServ.service_path

      # 加载过滤器
      filterArr = deps?.filters or null
      filterLen = filterArr?.length or 0
      if 0 isnt filterLen
        for currFilter in filterArr
          if currFilter?.filter_path
            depsArray.push currFilter.filter_path

      # 加载控制器
      ctrl_path = deps?.ctrl?.ctrl_path or null
      if null isnt ctrl_path
        depsArray.push deps.ctrl.ctrl_path
      return depsArray

    # 路由配置项 = 控制器 + 模板
    createCtrl = (deps) ->
      {
        templateUrl: deps['view_url']
        controller: deps['ctrl']['ctrl_name']
        resolve:
          delay: ($q, $rootScope) ->
            depsArray = getDepsArray deps
            depsLen = depsArray?.length or 0
            if 0 isnt depsLen
              deferred = $q.defer()
              # 动态加载-控制器文件
              require.async depsArray, () ->
                if '$apply' is $rootScope.$$phase or '$digest' is $rootScope.$$phase
                  deferred.resolve()
                else
                  $rootScope.$apply () ->
                    deferred.resolve()
              return deferred.promise
      }

    routeArray = [
      {
        'route_url': [ '/button' ]
        'deps':
          'view_url': 'view/button.html'
          'ctrl':
            'ctrl_name': 'buttonCtrl'
            'ctrl_path': 'mvc/ctrl/button_ctrl'
          'direct': [
            { 'direct_path': 'mvc/dirt/w_button' }
            { 'direct_path': 'mvc/dirt/w_toast' }
            { 'direct_path': 'mvc/dirt/w_listview' }
            { 'direct_path': 'mvc/dirt/w_header' }
            { 'direct_path': 'mvc/dirt/w_grid' }
          ]
          'service': [ 
            { 'service_path': 'mvc/serv/app_serv' } 
          ]
      }
      {
        'route_url': [ '/cell' ]
        'deps':
          'view_url': 'view/cell.html'
          'ctrl':
            'ctrl_name': 'cellCtrl'
            'ctrl_path': 'mvc/ctrl/cell_ctrl'
          'direct': [
            { 'direct_path': 'mvc/dirt/w_listview' }
            { 'direct_path': 'mvc/dirt/w_cmpt' }
            { 'direct_path': 'mvc/dirt/w_header' }
          ]
          'service': [
            { 'service_path': 'mvc/serv/cell_serv' } 
            { 'service_path': 'mvc/serv/app_serv' }
          ]
      }
      {
        'route_url': [ '/toast' ]
        'deps':
          'view_url': 'view/toast.html'
          'ctrl':
            'ctrl_name': 'toastCtrl'
            'ctrl_path': 'mvc/ctrl/toast_ctrl'
          'direct': [
            { 'direct_path': 'mvc/dirt/w_header' }
            { 'direct_path': 'mvc/dirt/w_button' }
            { 'direct_path': 'mvc/dirt/w_toast' }
          ]
          'service': [
            { 'service_path': 'mvc/serv/app_serv' }
          ]
      }
      {
        'route_url': [ '/dialog' ]
        'deps':
          'view_url': 'view/dialog.html'
          'ctrl':
            'ctrl_name': 'dialogCtrl'
            'ctrl_path': 'mvc/ctrl/dialog_ctrl'
          'direct': [
            { 'direct_path': 'mvc/dirt/w_header' }
            { 'direct_path': 'mvc/dirt/w_button' }
            { 'direct_path': 'mvc/dirt/w_dialog' }
          ]
          'service': [
            { 'service_path': 'mvc/serv/app_serv' }
          ]
      }
      {
        'route_url': [ '/progress' ]
        'deps':
          'view_url': 'view/progress.html'
          'ctrl':
            'ctrl_name': 'progressCtrl'
            'ctrl_path': 'mvc/ctrl/progress_ctrl'
          'direct': [
            { 'direct_path': 'mvc/dirt/w_header' }
            { 'direct_path': 'mvc/dirt/w_button' }
            { 'direct_path': 'mvc/dirt/w_progress' }
          ]
          'service': [
            { 'service_path': 'mvc/serv/app_serv' }
          ]
      }
      {
        'route_url': [ '/msg_page' ]
        'deps':
          'view_url': 'view/msg_page.html'
          'ctrl':
            'ctrl_name': 'msgPageCtrl'
            'ctrl_path': 'mvc/ctrl/msg_page_ctrl'
          'direct': [
            { 'direct_path': 'mvc/dirt/w_header' }
            { 'direct_path': 'mvc/dirt/w_msg_page' }
          ]
          'service': [
            { 'service_path': 'mvc/serv/app_serv' }
          ]
      }
      {
        'route_url': [ '/article' ]
        'deps':
          'view_url': 'view/article.html'
          'ctrl':
            'ctrl_name': 'articleCtrl'
            'ctrl_path': 'mvc/ctrl/article_ctrl'
          'direct': [
            { 'direct_path': 'mvc/dirt/w_header' }
          ]
          'service': [ 
            { 'service_path': 'mvc/serv/app_serv' } 
          ]
      }
      {
        'route_url': [ '/action_sheet' ]
        'deps':
          'view_url': 'view/action_sheet.html'
          'ctrl':
            'ctrl_name': 'actionSheetCtrl'
            'ctrl_path': 'mvc/ctrl/action_sheet_ctrl'
          'direct': [
            { 'direct_path': 'mvc/dirt/w_header' }
            { 'direct_path': 'mvc/dirt/w_button' }
            { 'direct_path': 'mvc/dirt/w_action_sheet' }
          ]
          'service': [
            { 'service_path': 'mvc/serv/app_serv' }
          ]
      }
      {
        'route_url': [ '/icons' ]
        'deps':
          'view_url': 'view/icons.html'
          'ctrl':
            'ctrl_name': 'iconsCtrl'
            'ctrl_path': 'mvc/ctrl/icons_ctrl'
          'direct': [
            { 'direct_path': 'mvc/dirt/w_header' }
          ]
          'service': [
            { 'service_path': 'mvc/serv/app_serv' }
          ]
      }
      {
        'route_url': [ '/' ]
        'deps':
          'view_url': 'view/main.html'
          'ctrl':
            'ctrl_name': 'mainCtrl'
            'ctrl_path': 'mvc/ctrl/main_ctrl'
          'direct': [
            { 'direct_path': 'mvc/dirt/w_toast' }
            { 'direct_path': 'mvc/dirt/w_header' }
            { 'direct_path': 'mvc/dirt/w_grid' }
          ]
          'service': [ 
            { 'service_path': 'mvc/serv/app_serv' } 
          ]
      }
    ]

    $routeProvider.otherwise {redirectTo : '/'}
    routeLen = routeArray?.length or 0
    if 0 isnt routeLen
      for currRoute  in routeArray
        urlLen = currRoute?.route_url?.length or 0
        if 0 is urlLen
          return false
        else
          $routeProvider
            .when currRoute['route_url'][0], createCtrl currRoute['deps']