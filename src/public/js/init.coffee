seajs.config
  base: '.'
  plugins: ['shim']
  alias:
    'zepto': 'comps/zepto.min'
    'angular': 'comps/angular.min'
    'angular-route': 'comps/angular-route.min'
    'angular-touch': 'comps/angular-touch.min'
    # ng 实例化主模块
    'app': 'js/ns'
    # index 初始化路由模块
    'route' : 'js/route'

  charset: 'utf-8'
  timeout: 20000
  debug: false

# use是下载对应的js文件，回调函数中是注入后的实例名称
seajs.use ['zepto', 'angular', 'angular-route', 'app', 'route'], ($, angular) ->
  $ () ->
    # 加载ng主模块，单页应用，只有一个模块
    # 其余的功能(filter+service+controller)
    # 都是要require('app')ng主模块，页面只启动主
    # 模块+路由模块，路由模块分发到对应控制器模块
    # (跳转到对应模块时才下载，也就是按需加载)
    angular.bootstrap $('#view_cont'), ['app']