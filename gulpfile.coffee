fs = require 'fs'
_ = require 'lodash'

gulp = require 'gulp'

clean = require 'gulp-clean'
jade =  require 'gulp-jade'
coffee = require 'gulp-coffee'
stylus = require 'gulp-stylus'


jade_src = 'src/**/*.jade'
stylus_src = 'src/**/*.styl'
coffee_src = 'src/**/*.coffee'

static_array = [
  'src/**/*.js'
  'src/**/*.tpl'
  'src/**/*.png'
  'src/**/*.jpg'
  'src/**/*.css'
  'src/**/*.html'
]

compile_array = [coffee_src, jade_src, stylus_src]
new_array = _.concat compile_array, static_array

gulp.task 'scripts', () ->
  gulp.src coffee_src
    .pipe coffee()
    .pipe gulp.dest 'dest'

gulp.task 'html', () ->
  gulp.src jade_src
    .pipe jade()
    .pipe gulp.dest 'dest'

gulp.task 'css', () ->
  gulp.src stylus_src
    .pipe stylus()
    .pipe gulp.dest 'dest'

gulp.task 'copy', () ->
  gulp.src static_array
    .pipe gulp.dest 'dest'

gulp.task 'clean', () ->
  gulp.src 'dest'
    .pipe clean()

gulp.task 'watch',  () ->
  gulp.watch new_array, ['scripts', 'html', 'css',  'copy']

gulp.task 'default', ['scripts', 'html', 'css',  'copy', 'watch']