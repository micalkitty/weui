(function() {
  var app, express, moment, path;

  path = require('path');

  moment = require('moment');

  express = require('express');

  app = express();

  app.set('views', __dirname + "/public/view");

  app.use(express["static"](__dirname + "/public"));

  app.get('/', function(req, res) {
    return res.sendFile(__dirname + "/public/view/index.html");
  });

  app.listen(3000, function() {
    console.log('Server is listening on port 3000');
  });

}).call(this);
