(function() {
  define(function(require, exports, module) {
    var app;
    app = require('app');
    return app.config(function($routeProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $httpProvider) {
      var createCtrl, currRoute, getDepsArray, i, len, ref, routeArray, routeLen, urlLen;
      app.register = {
        controller: $controllerProvider.register,
        directive: $compileProvider.directive,
        filter: $filterProvider.register,
        factory: $provide.factory,
        service: $provide.service
      };
      getDepsArray = function(deps) {
        var ctrl_path, currDirt, currFilter, currServ, depsArray, dirtArr, dirtLen, filterArr, filterLen, i, j, k, len, len1, len2, ref, servArr, servLen;
        depsArray = [];
        if (!deps) {
          return [];
        }
        dirtArr = (deps != null ? deps.direct : void 0) || null;
        dirtLen = (dirtArr != null ? dirtArr.length : void 0) || 0;
        if (0 !== dirtLen) {
          for (i = 0, len = dirtArr.length; i < len; i++) {
            currDirt = dirtArr[i];
            if (currDirt != null ? currDirt.direct_path : void 0) {
              depsArray.push(currDirt.direct_path);
            }
          }
        }
        servArr = (deps != null ? deps.service : void 0) || null;
        servLen = (servArr != null ? servArr.length : void 0) || 0;
        if (0 !== servLen) {
          for (j = 0, len1 = servArr.length; j < len1; j++) {
            currServ = servArr[j];
            if (currServ != null ? currServ.service_path : void 0) {
              depsArray.push(currServ.service_path);
            }
          }
        }
        filterArr = (deps != null ? deps.filters : void 0) || null;
        filterLen = (filterArr != null ? filterArr.length : void 0) || 0;
        if (0 !== filterLen) {
          for (k = 0, len2 = filterArr.length; k < len2; k++) {
            currFilter = filterArr[k];
            if (currFilter != null ? currFilter.filter_path : void 0) {
              depsArray.push(currFilter.filter_path);
            }
          }
        }
        ctrl_path = (deps != null ? (ref = deps.ctrl) != null ? ref.ctrl_path : void 0 : void 0) || null;
        if (null !== ctrl_path) {
          depsArray.push(deps.ctrl.ctrl_path);
        }
        return depsArray;
      };
      createCtrl = function(deps) {
        return {
          templateUrl: deps['view_url'],
          controller: deps['ctrl']['ctrl_name'],
          resolve: {
            delay: function($q, $rootScope) {
              var deferred, depsArray, depsLen;
              depsArray = getDepsArray(deps);
              depsLen = (depsArray != null ? depsArray.length : void 0) || 0;
              if (0 !== depsLen) {
                deferred = $q.defer();
                require.async(depsArray, function() {
                  if ('$apply' === $rootScope.$$phase || '$digest' === $rootScope.$$phase) {
                    return deferred.resolve();
                  } else {
                    return $rootScope.$apply(function() {
                      return deferred.resolve();
                    });
                  }
                });
                return deferred.promise;
              }
            }
          }
        };
      };
      routeArray = [
        {
          'route_url': ['/button'],
          'deps': {
            'view_url': 'view/button.html',
            'ctrl': {
              'ctrl_name': 'buttonCtrl',
              'ctrl_path': 'mvc/ctrl/button_ctrl'
            },
            'direct': [
              {
                'direct_path': 'mvc/dirt/w_button'
              }, {
                'direct_path': 'mvc/dirt/w_toast'
              }, {
                'direct_path': 'mvc/dirt/w_listview'
              }, {
                'direct_path': 'mvc/dirt/w_header'
              }, {
                'direct_path': 'mvc/dirt/w_grid'
              }
            ],
            'service': [
              {
                'service_path': 'mvc/serv/app_serv'
              }
            ]
          }
        }, {
          'route_url': ['/cell'],
          'deps': {
            'view_url': 'view/cell.html',
            'ctrl': {
              'ctrl_name': 'cellCtrl',
              'ctrl_path': 'mvc/ctrl/cell_ctrl'
            },
            'direct': [
              {
                'direct_path': 'mvc/dirt/w_listview'
              }, {
                'direct_path': 'mvc/dirt/w_cmpt'
              }, {
                'direct_path': 'mvc/dirt/w_header'
              }
            ],
            'service': [
              {
                'service_path': 'mvc/serv/cell_serv'
              }, {
                'service_path': 'mvc/serv/app_serv'
              }
            ]
          }
        }, {
          'route_url': ['/toast'],
          'deps': {
            'view_url': 'view/toast.html',
            'ctrl': {
              'ctrl_name': 'toastCtrl',
              'ctrl_path': 'mvc/ctrl/toast_ctrl'
            },
            'direct': [
              {
                'direct_path': 'mvc/dirt/w_header'
              }, {
                'direct_path': 'mvc/dirt/w_button'
              }, {
                'direct_path': 'mvc/dirt/w_toast'
              }
            ],
            'service': [
              {
                'service_path': 'mvc/serv/app_serv'
              }
            ]
          }
        }, {
          'route_url': ['/dialog'],
          'deps': {
            'view_url': 'view/dialog.html',
            'ctrl': {
              'ctrl_name': 'dialogCtrl',
              'ctrl_path': 'mvc/ctrl/dialog_ctrl'
            },
            'direct': [
              {
                'direct_path': 'mvc/dirt/w_header'
              }, {
                'direct_path': 'mvc/dirt/w_button'
              }, {
                'direct_path': 'mvc/dirt/w_dialog'
              }
            ],
            'service': [
              {
                'service_path': 'mvc/serv/app_serv'
              }
            ]
          }
        }, {
          'route_url': ['/progress'],
          'deps': {
            'view_url': 'view/progress.html',
            'ctrl': {
              'ctrl_name': 'progressCtrl',
              'ctrl_path': 'mvc/ctrl/progress_ctrl'
            },
            'direct': [
              {
                'direct_path': 'mvc/dirt/w_header'
              }, {
                'direct_path': 'mvc/dirt/w_button'
              }, {
                'direct_path': 'mvc/dirt/w_progress'
              }
            ],
            'service': [
              {
                'service_path': 'mvc/serv/app_serv'
              }
            ]
          }
        }, {
          'route_url': ['/msg_page'],
          'deps': {
            'view_url': 'view/msg_page.html',
            'ctrl': {
              'ctrl_name': 'msgPageCtrl',
              'ctrl_path': 'mvc/ctrl/msg_page_ctrl'
            },
            'direct': [
              {
                'direct_path': 'mvc/dirt/w_header'
              }, {
                'direct_path': 'mvc/dirt/w_msg_page'
              }
            ],
            'service': [
              {
                'service_path': 'mvc/serv/app_serv'
              }
            ]
          }
        }, {
          'route_url': ['/article'],
          'deps': {
            'view_url': 'view/article.html',
            'ctrl': {
              'ctrl_name': 'articleCtrl',
              'ctrl_path': 'mvc/ctrl/article_ctrl'
            },
            'direct': [
              {
                'direct_path': 'mvc/dirt/w_header'
              }
            ],
            'service': [
              {
                'service_path': 'mvc/serv/app_serv'
              }
            ]
          }
        }, {
          'route_url': ['/action_sheet'],
          'deps': {
            'view_url': 'view/action_sheet.html',
            'ctrl': {
              'ctrl_name': 'actionSheetCtrl',
              'ctrl_path': 'mvc/ctrl/action_sheet_ctrl'
            },
            'direct': [
              {
                'direct_path': 'mvc/dirt/w_header'
              }, {
                'direct_path': 'mvc/dirt/w_button'
              }, {
                'direct_path': 'mvc/dirt/w_action_sheet'
              }
            ],
            'service': [
              {
                'service_path': 'mvc/serv/app_serv'
              }
            ]
          }
        }, {
          'route_url': ['/icons'],
          'deps': {
            'view_url': 'view/icons.html',
            'ctrl': {
              'ctrl_name': 'iconsCtrl',
              'ctrl_path': 'mvc/ctrl/icons_ctrl'
            },
            'direct': [
              {
                'direct_path': 'mvc/dirt/w_header'
              }
            ],
            'service': [
              {
                'service_path': 'mvc/serv/app_serv'
              }
            ]
          }
        }, {
          'route_url': ['/'],
          'deps': {
            'view_url': 'view/main.html',
            'ctrl': {
              'ctrl_name': 'mainCtrl',
              'ctrl_path': 'mvc/ctrl/main_ctrl'
            },
            'direct': [
              {
                'direct_path': 'mvc/dirt/w_toast'
              }, {
                'direct_path': 'mvc/dirt/w_header'
              }, {
                'direct_path': 'mvc/dirt/w_grid'
              }
            ],
            'service': [
              {
                'service_path': 'mvc/serv/app_serv'
              }
            ]
          }
        }
      ];
      $routeProvider.otherwise({
        redirectTo: '/'
      });
      routeLen = (routeArray != null ? routeArray.length : void 0) || 0;
      if (0 !== routeLen) {
        for (i = 0, len = routeArray.length; i < len; i++) {
          currRoute = routeArray[i];
          urlLen = (currRoute != null ? (ref = currRoute.route_url) != null ? ref.length : void 0 : void 0) || 0;
          if (0 === urlLen) {
            return false;
          } else {
            $routeProvider.when(currRoute['route_url'][0], createCtrl(currRoute['deps']));
          }
        }
      }
    });
  });

}).call(this);
