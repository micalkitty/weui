(function() {
  seajs.config({
    base: '.',
    plugins: ['shim'],
    alias: {
      'zepto': 'comps/zepto.min',
      'angular': 'comps/angular.min',
      'angular-route': 'comps/angular-route.min',
      'angular-touch': 'comps/angular-touch.min',
      'app': 'js/ns',
      'route': 'js/route'
    },
    charset: 'utf-8',
    timeout: 20000,
    debug: false
  });

  seajs.use(['zepto', 'angular', 'angular-route', 'app', 'route'], function($, angular) {
    return $(function() {
      return angular.bootstrap($('#view_cont'), ['app']);
    });
  });

}).call(this);
