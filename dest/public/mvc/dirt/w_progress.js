(function() {
  define(function(require, exports, module) {
    var app, progressFunc;
    app = require('app');
    progressFunc = function($timeout) {
      return {
        restrict: "E",
        transclude: true,
        scope: {
          cpercent: '=',
          cbf: "&"
        },
        templateUrl: 'tpl/progress_tpl.html',
        link: function(scope, element, attrs) {
          return scope.pgCancel = function() {
            if (scope.cbf) {
              return scope.cbf();
            } else {
              return false;
            }
          };
        }
      };
    };
    progressFunc.$inject = ['$timeout'];
    return app.register.directive('wprogress', progressFunc);
  });

}).call(this);
