(function() {
  define(function(require, exports, module) {
    var actionSheetFunc, app;
    app = require('app');
    actionSheetFunc = function($timeout) {
      return {
        restrict: "E",
        transclude: true,
        scope: {
          actionitems: '=',
          astatus: '=',
          cbf: "&"
        },
        templateUrl: 'tpl/action_sheet_tpl.html',
        link: function(scope, element, attrs) {
          return scope.item_clk = function(idx) {
            if (scope.cbf) {
              return scope.cbf({
                'action_index': idx
              });
            }
          };
        }
      };
    };
    actionSheetFunc.$inject = ['$timeout'];
    return app.register.directive('wactionsheet', actionSheetFunc);
  });

}).call(this);
