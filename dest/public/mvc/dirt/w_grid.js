(function() {
  define(function(require, exports, module) {
    var app, gridFunc;
    app = require('app');
    gridFunc = function($timeout) {
      return {
        restrict: "E",
        transclude: true,
        scope: {
          griditems: '=',
          cbf: "&"
        },
        templateUrl: 'tpl/grid_tpl.html',
        link: function(scope, element, attrs) {}
      };
    };
    gridFunc.$inject = ['$timeout'];
    return app.register.directive('wgrid', gridFunc);
  });

}).call(this);
