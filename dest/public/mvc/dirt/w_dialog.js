(function() {
  define(function(require, exports, module) {
    var app, dialogFunc;
    app = require('app');
    dialogFunc = function($timeout) {
      return {
        restrict: "E",
        transclude: true,
        scope: {
          ctitle: '=',
          cdlgtype: '=',
          okclk: '&',
          cancelclk: '&',
          cstatus: '=',
          cbf: "&"
        },
        templateUrl: 'tpl/dialog_tpl.html',
        link: function(scope, element, attrs) {}
      };
    };
    dialogFunc.$inject = ['$timeout'];
    return app.register.directive('wdialog', dialogFunc);
  });

}).call(this);
