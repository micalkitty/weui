(function() {
  define(function(require, exports, module) {
    var app, msgPageFunc;
    app = require('app');
    msgPageFunc = function($timeout) {
      return {
        restrict: "E",
        transclude: true,
        scope: {
          msgtitle: '=',
          msgtype: '=',
          msgdesc: '=',
          okclk: '&',
          cancelclk: '&',
          detailurl: '=',
          cbf: "&"
        },
        templateUrl: 'tpl/msg_page_tpl.html',
        link: function(scope, element, attrs) {}
      };
    };
    msgPageFunc.$inject = ['$timeout'];
    return app.register.directive('wmsgpage', msgPageFunc);
  });

}).call(this);
