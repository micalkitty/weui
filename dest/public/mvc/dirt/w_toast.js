(function() {
  define(function(require, exports, module) {
    var app, toastFunc;
    app = require('app');
    toastFunc = function($timeout) {
      return {
        restrict: "E",
        transclude: true,
        scope: {
          ctoasttype: '=',
          cstatus: '=',
          cbf: "&"
        },
        templateUrl: 'tpl/toast_tpl.html',
        link: function(scope, element, attrs) {}
      };
    };
    toastFunc.$inject = ['$timeout'];
    return app.register.directive('wtoast', toastFunc);
  });

}).call(this);
