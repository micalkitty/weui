(function() {
  define(function(require, exports, module) {
    var app, wboxFunc;
    app = require('app');
    wboxFunc = function($timeout) {
      return {
        restrict: 'E',
        transclude: true,
        scope: {
          ctype: '=',
          citems: '=',
          clabel: '=',
          cfieldname: '=',
          checkitems: '=',
          radioitems: '=',
          selectitems: '=',
          switchitems: '=',
          textitems: '=',
          vcodeitems: '=',
          vimgurl: '@',
          textareaitems: '=',
          dateitems: '=',
          cplaceholder: '=',
          clisttype: '=',
          clistitems: '=',
          iscell: '=',
          cbf: '&'
        },
        templateUrl: 'tpl/cmpt_tpl.html',
        link: function(scope, element, attrs) {
          var checkLen, ref;
          if ('checkbox' === '' + scope.ctype) {
            checkLen = (scope != null ? (ref = scope.checkitems) != null ? ref.length : void 0 : void 0) || 0;
            if (0 === checkLen) {
              scope.checkitems = scope.citems;
            }
          }
          scope.findByKey = function(arr, key) {
            var a, i, len, len1;
            len = (arr != null ? arr.length : void 0) || 0;
            if (0 === len || !key) {
              return null;
            }
            for (i = 0, len1 = arr.length; i < len1; i++) {
              a = arr[i];
              if ('' + key === '' + a['key']) {
                return a;
              }
            }
          };
          scope.checkClick = function() {
            return scope.cbf({
              'check_data': scope.checkitems
            });
          };
          scope.radioClick = function() {
            var radioObj;
            radioObj = scope.findByKey(scope.citems, scope.radioitems);
            return scope.cbf({
              'radio_data': radioObj
            });
          };
          scope.selectChange = function() {
            var selectObj;
            selectObj = scope.findByKey(scope.citems, scope.selectitems);
            return scope.cbf({
              'select_data': selectObj
            });
          };
          scope.switchClick = function() {
            return scope.cbf({
              'switch_data': scope.switchitems
            });
          };
          scope.textChange = function() {
            return scope.cbf({
              'text_data': scope.textitems
            });
          };
          scope.vcodeChange = function() {
            return scope.cbf({
              'vcode_data': scope.vcodeitems
            });
          };
          scope.textareaChange = function() {
            return scope.cbf({
              'textarea_data': scope.textareaitems
            });
          };
          return scope.dateChange = function() {
            return scope.cbf({
              'date_data': scope.dateitems
            });
          };
        }
      };
    };
    wboxFunc.$inject = ['$timeout'];
    return app.register.directive('wbox', wboxFunc);
  });

}).call(this);
