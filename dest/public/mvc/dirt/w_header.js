(function() {
  define(function(require, exports, module) {
    var app, viewFunc;
    app = require('app');
    viewFunc = function($timeout, $window) {
      return {
        restrict: "E",
        transclude: true,
        scope: {
          ctitle: '=',
          cbuttons: '=',
          cbf: "&"
        },
        templateUrl: 'tpl/header_tpl.html',
        link: function(scope, element, attrs) {
          return scope.headerBtnClk = function(url) {
            if (url && "" !== url) {
              return $window.location.href = url;
            }
          };
        }
      };
    };
    viewFunc.$inject = ['$timeout', '$window'];
    return app.register.directive('wheader', viewFunc);
  });

}).call(this);
