(function() {
  define(function(require, exports, module) {
    var app, viewFunc;
    app = require('app');
    viewFunc = function($timeout) {
      return {
        restrict: "E",
        transclude: true,
        scope: {
          clisttype: '=',
          clistitems: '=',
          cbf: "&"
        },
        templateUrl: 'tpl/listview_tpl.html',
        link: function(scope, element, attrs) {}
      };
    };
    viewFunc.$inject = ['$timeout'];
    return app.register.directive('wlistview', viewFunc);
  });

}).call(this);
