(function() {
  define(function(require, exports, module) {
    var app, buttonFunc;
    app = require('app');
    buttonFunc = function($timeout) {
      return {
        restrict: "E",
        transclude: true,
        scope: {
          ctitle: '=',
          cstyle: '=',
          cinner: '=',
          cbf: "&"
        },
        templateUrl: 'tpl/button_tpl.html',
        link: function(scope, element, attrs) {
          return scope.btnClk = function() {
            if ('b' === '' + scope.cstyle || 'd' === '' + scope.cstyle || 'f' === '' + scope.cstyle) {
              return false;
            } else {
              if (scope.cbf) {
                return scope.cbf();
              } else {
                return false;
              }
            }
          };
        }
      };
    };
    buttonFunc.$inject = ['$timeout'];
    return app.register.directive('wbutton', buttonFunc);
  });

}).call(this);
