(function() {
  define(function(require, exports, module) {
    var $, app, appFunc;
    app = require('app');
    $ = require('zepto');
    appFunc = function($http, $location, $timeout) {
      var service;
      service = {
        loadOk: function() {
          $('#app_load').hide();
        }
      };
      return service;
    };
    appFunc.$inject = ['$http', '$location', '$timeout'];
    return app.register.service('appServ', appFunc);
  });

}).call(this);
