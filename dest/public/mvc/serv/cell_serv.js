(function() {
  define(function(require, exports, module) {
    var app, cellFunc;
    app = require('app');
    cellFunc = function($http, $location, $timeout) {
      var service;
      service = {
        getQqVal: function() {
          return '254475842';
        }
      };
      return service;
    };
    cellFunc.$inject = ['$http', '$location', '$timeout'];
    return app.register.service('cellServ', cellFunc);
  });

}).call(this);
