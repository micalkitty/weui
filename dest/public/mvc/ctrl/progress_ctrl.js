(function() {
  define(function(require, exports, module) {
    var app, progressFunc;
    app = require('app');
    progressFunc = function($scope, $http, $location, $timeout, $interval, appServ) {
      $scope.vars = {
        header_btns: [],
        percent1: 0,
        percent2: 50,
        percent3: 80
      };
      $scope.act = {
        upload: function() {
          $scope.vars.percent1 = 0;
          $scope.vars.percent2 = 0;
          $scope.vars.percent3 = 0;
          $interval(function() {
            if ($scope.vars.percent1 < 100) {
              return $scope.vars.percent1 += 1;
            } else {
              return $scope.vars.percent1 = 0;
            }
          }, 30);
          $interval(function() {
            if ($scope.vars.percent2 < 100) {
              return $scope.vars.percent2 += 1;
            } else {
              return $scope.vars.percent2 = 0;
            }
          }, 30);
          return $interval(function() {
            if ($scope.vars.percent3 < 100) {
              return $scope.vars.percent3 += 1;
            } else {
              return $scope.vars.percent3 = 0;
            }
          }, 30);
        },
        init: function() {
          $scope.vars.header_btns = [
            {
              'title': '返回',
              'goto_url': '#/'
            }
          ];
          return appServ.loadOk();
        }
      };
      return $scope.act.init();
    };
    progressFunc.$inject = ['$scope', '$http', '$location', '$timeout', '$interval', 'appServ'];
    return app.register.controller('progressCtrl', progressFunc);
  });

}).call(this);
