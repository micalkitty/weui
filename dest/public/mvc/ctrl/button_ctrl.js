(function() {
  define(function(require, exports, module) {
    var app, buttonFunc;
    app = require('app');
    buttonFunc = function($scope, $http, $location, $timeout, appServ) {
      $scope.vars = {
        header_btns: []
      };
      $scope.act = {
        init: function() {
          $scope.vars.header_btns = [
            {
              'title': '返回',
              'goto_url': '#/'
            }
          ];
          return appServ.loadOk();
        }
      };
      return $scope.act.init();
    };
    buttonFunc.$inject = ['$scope', '$http', '$location', '$timeout', 'appServ'];
    return app.register.controller('buttonCtrl', buttonFunc);
  });

}).call(this);
