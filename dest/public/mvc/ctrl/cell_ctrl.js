(function() {
  define(function(require, exports, module) {
    var app, cellFunc;
    app = require('app');
    cellFunc = function($scope, $http, $location, $timeout, cellServ, appServ) {
      $scope.vars = {
        lxfs_val: {},
        lxfs_items: {},
        sex_val: {},
        sex_items: {},
        love_val: {},
        love_items: {},
        switch_val: false,
        qq_val: '',
        vcode_val: '',
        remark_val: '',
        rq_val: '',
        prod_items: {},
        header_btns: []
      };
      $scope.act = {
        lxfsChange: function(val) {
          return console.log('lxfs:', val);
        },
        sexChange: function(val) {
          return console.log('sex:', val);
        },
        loveChange: function(val) {
          return console.log('love:', JSON.stringify(val));
        },
        switchChange: function(val) {
          return console.log('switch:', JSON.stringify(val));
        },
        qqChange: function(val) {
          return console.log('qq:', JSON.stringify(val));
        },
        vcodeChange: function(val) {
          return console.log('vcode:', JSON.stringify(val));
        },
        remarkChange: function(val) {
          return console.log('remark:', JSON.stringify(val));
        },
        rqChange: function(val) {
          return console.log('rq:', JSON.stringify(val));
        },
        init: function() {
          $scope.vars.lxfs_val = {};
          $scope.vars.lxfs_items = [
            {
              'key': '1',
              'value': '微信号',
              'group': '1'
            }, {
              'key': '2',
              'value': 'QQ号',
              'group': '2'
            }, {
              'key': '3',
              'value': 'Email',
              'group': '3'
            }
          ];
          $scope.vars.sex_val = {
            'key': '1',
            'value': '男',
            'group': '1'
          };
          $scope.vars.sex_items = [
            {
              'key': '1',
              'value': '男',
              'group': '1'
            }, {
              'key': '2',
              'value': '女',
              'group': '2'
            }
          ];
          $scope.vars.love_val = [];
          $scope.vars.love_items = [
            {
              'key': '1',
              'value': '旅游',
              'group': '1',
              'checked': false
            }, {
              'key': '2',
              'value': '篮球',
              'group': '2',
              'checked': false
            }, {
              'key': '3',
              'value': '足球',
              'group': '3',
              'checked': false
            }
          ];
          $scope.vars.prod_items = [
            {
              'title': '产品1',
              'remark': '介绍产品1',
              'img_url': '',
              'goto_url': ''
            }, {
              'title': '产品2',
              'remark': '介绍产品2',
              'img_url': '',
              'goto_url': ''
            }, {
              'title': '产品3',
              'remark': '介绍产品3',
              'img_url': '',
              'goto_url': ''
            }
          ];
          $scope.vars.header_btns = [
            {
              'title': '返回',
              'goto_url': '#/'
            }
          ];
          $scope.vars.qq_val = cellServ.getQqVal();
          return appServ.loadOk();
        }
      };
      return $scope.act.init();
    };
    cellFunc.$inject = ['$scope', '$http', '$location', '$timeout', 'cellServ', 'appServ'];
    return app.register.controller('cellCtrl', cellFunc);
  });

}).call(this);
