(function() {
  define(function(require, exports, module) {
    var app, msgPageFunc;
    app = require('app');
    msgPageFunc = function($scope, $http, $location, $timeout, appServ) {
      $scope.vars = {
        header_btns: []
      };
      $scope.act = {
        msgOkClk: function() {
          console.log('click msg ok');
          return $scope.vars.msgStatus = false;
        },
        msgCancelClk: function() {
          console.log('click msg cancel');
          return $scope.vars.msgStatus = false;
        },
        init: function() {
          $scope.vars.header_btns = [
            {
              'title': '返回',
              'goto_url': '#/'
            }
          ];
          return appServ.loadOk();
        }
      };
      return $scope.act.init();
    };
    msgPageFunc.$inject = ['$scope', '$http', '$location', '$timeout', 'appServ'];
    return app.register.controller('msgPageCtrl', msgPageFunc);
  });

}).call(this);
