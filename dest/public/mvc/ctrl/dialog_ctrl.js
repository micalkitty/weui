(function() {
  define(function(require, exports, module) {
    var app, dialogFunc;
    app = require('app');
    dialogFunc = function($scope, $http, $location, $timeout, appServ) {
      $scope.vars = {
        header_btns: [],
        dlgStatus: false,
        dlgStatus2: false
      };
      $scope.act = {
        showDlg: function() {
          return $scope.vars.dlgStatus = true;
        },
        ok: function() {
          console.log('click ok');
          return $scope.vars.dlgStatus = false;
        },
        cancel: function() {
          $scope.vars.dlgStatus = false;
          return console.log('click cancel');
        },
        showDlg2: function() {
          return $scope.vars.dlgStatus2 = true;
        },
        ok2: function() {
          console.log('click ok');
          return $scope.vars.dlgStatus2 = false;
        },
        cancel2: function() {
          $scope.vars.dlgStatus2 = false;
          return console.log('click cancel');
        },
        init: function() {
          $scope.vars.header_btns = [
            {
              'title': '返回',
              'goto_url': '#/'
            }
          ];
          return appServ.loadOk();
        }
      };
      return $scope.act.init();
    };
    dialogFunc.$inject = ['$scope', '$http', '$location', '$timeout', 'appServ'];
    return app.register.controller('dialogCtrl', dialogFunc);
  });

}).call(this);
