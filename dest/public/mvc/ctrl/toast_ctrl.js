(function() {
  define(function(require, exports, module) {
    var app, toastFunc;
    app = require('app');
    toastFunc = function($scope, $http, $location, $timeout, appServ) {
      $scope.vars = {
        header_btns: [],
        toastStatus: false,
        toastLoadStatus: false
      };
      $scope.act = {
        showToast: function() {
          $scope.vars.toastStatus = true;
          return $timeout(function() {
            return $scope.vars.toastStatus = false;
          }, 5000);
        },
        showLoadToast: function() {
          $scope.vars.toastLoadStatus = true;
          return $timeout(function() {
            return $scope.vars.toastLoadStatus = false;
          }, 5000);
        },
        init: function() {
          $scope.vars.header_btns = [
            {
              'title': '返回',
              'goto_url': '#/'
            }
          ];
          return appServ.loadOk();
        }
      };
      return $scope.act.init();
    };
    toastFunc.$inject = ['$scope', '$http', '$location', '$timeout', 'appServ'];
    return app.register.controller('toastCtrl', toastFunc);
  });

}).call(this);
