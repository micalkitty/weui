(function() {
  define(function(require, exports, module) {
    var $, app, mainFunc;
    app = require('app');
    $ = require('zepto');
    mainFunc = function($scope, $http, $location, $timeout, appServ) {
      $scope.vars = {
        grid_items: []
      };
      $scope.act = {
        init: function() {
          $scope.vars.grid_items = [
            {
              'text': 'Button',
              'goto_url': '#/button',
              'img_url': 'img/icon_nav_button.png'
            }, {
              'text': 'Cell',
              'goto_url': '#/cell',
              'img_url': 'img/icon_nav_cell.png'
            }, {
              'text': 'Toast',
              'goto_url': '#/toast',
              'img_url': 'img/icon_nav_toast.png'
            }, {
              'text': 'Dialog',
              'goto_url': '#/dialog',
              'img_url': 'img/icon_nav_dialog.png'
            }, {
              'text': 'progress',
              'goto_url': '#/progress',
              'img_url': 'img/icon_nav_button.png'
            }, {
              'text': 'Msg',
              'goto_url': '#/msg_page',
              'img_url': 'img/icon_nav_msg.png'
            }, {
              'text': 'Article',
              'goto_url': '#/article',
              'img_url': 'img/icon_nav_article.png'
            }, {
              'text': 'ActionSheet',
              'goto_url': '#/action_sheet',
              'img_url': 'img/icon_nav_actionSheet.png'
            }, {
              'text': 'icons',
              'goto_url': '#/icons',
              'img_url': 'img/icon_nav_icons.png'
            }
          ];
          return appServ.loadOk();
        }
      };
      return $scope.act.init();
    };
    mainFunc.$inject = ['$scope', '$http', '$location', '$timeout', 'appServ'];
    return app.register.controller('mainCtrl', mainFunc);
  });

}).call(this);
