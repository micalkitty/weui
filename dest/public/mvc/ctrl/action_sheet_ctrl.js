(function() {
  define(function(require, exports, module) {
    var actionSheetFunc, app;
    app = require('app');
    actionSheetFunc = function($scope, $http, $location, $timeout, appServ) {
      $scope.vars = {
        header_btns: [],
        asStatus: false,
        asMenus: []
      };
      $scope.act = {
        showActionSheet: function() {
          return $scope.vars.asStatus = true;
        },
        actionSheetClk: function(idx) {
          return console.log('idx:', idx);
        },
        init: function() {
          $scope.vars.header_btns = [
            {
              'title': '返回',
              'goto_url': '#/'
            }
          ];
          $scope.vars.asMenus = [
            {
              'title': '示例菜单'
            }, {
              'title': '示例菜单'
            }, {
              'title': '示例菜单'
            }, {
              'title': '示例菜单'
            }
          ];
          return appServ.loadOk();
        }
      };
      return $scope.act.init();
    };
    actionSheetFunc.$inject = ['$scope', '$http', '$location', '$timeout', 'appServ'];
    return app.register.controller('actionSheetCtrl', actionSheetFunc);
  });

}).call(this);
